# CentOS Alternative Images SIG KIWI Descriptions

This contains the KIWI descriptions for building the Alternative Images SIG spin.

Individual branches are targeted for specific versions of CentOS Stream, with the naming convention
of `cXs` with `X` being the version number. For example, `c9s` is for targeting CentOS Stream 9.

Each branch (referring to a distribution) contains a README file for how to use the files contained.
